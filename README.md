### What is this repository for? ###

Repo holds the support for the new display resolution of the Onda V975M V1, V3 and V4.
For complete operation you also need the device folder for creation of TWRP for Onda V975M which is located in a second repo.
Both repos are pulled using below manifest file.

### How do I get set up? ###

Copy this to new file "onda_k12_MA975M8.xml" and save the file in "~/[CM_source_files]/.repo/local_manifests"

<?xml version="1.0" encoding="UTF-8"?>
<manifest>

    <remote name="bitbucket" 
    fetch="https://bitbucket.org"/>

    <project name="SciBee/device_onda_v975m_twrp" path="device/onda/k12_MA975M8" remote="bitbucket" revision="master"/>
    <project name="SciBee/twrp_gui_onda_v975m" path="bootable/recovery/gui/devices/2048x1536" remote="bitbucket" revision="master"/>
    
</manifest>